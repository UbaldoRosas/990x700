var adDiv;

function initEB() {
  if (!EB.isInitialized()) {
    EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
  } else {
    startAd();
  }
}

function startAd() {
  adDiv = document.getElementById("mu_wrapper");

  addEventListeners();

  MU.init();
}

function addEventListeners() {
  document.getElementById("btnAction").addEventListener("click", clickthrough);
}

function clickthrough() {
  console.log('hola');
  EB.clickthrough();
}

window.addEventListener("load", initEB);
